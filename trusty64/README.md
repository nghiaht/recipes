#Private Local Kubernetes Cluster v0.0.1_2016_01_29

One master node, and two more worker nodes.

Tested on a Windows host machine, Vagrant 1.8.1 preinstalled, VirtualBox 5.0.

* vagrant up

  Master: 172.28.0.10

* vagrant up minion1

  Minion1: 172.28.0.11

* vagrant up minion2

  Minion2: 172.28.0.12

* These nodes are host-access only.

  We can forward some ports that their services run and access via host's IP address:<port_forwared> (do port-forwarding in Virtualbox GUI, or Vagranfile).

* vagrant up <vm_name> [--no-provision]

  If you just want to start a raw simple machine.

* vagrant provision <vm_name>

  Re-run provision definitions in Vagrantfile.

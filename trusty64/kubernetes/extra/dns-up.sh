echo "-- dns-up -->"
echo "Prepare configurations"

cd /vagrant/kubernetes/extra

export DNS_REPLICAS=1
export DNS_DOMAIN=cluster.local # specify in startup parameter `--cluster-domain` for containerized kubelet
export DNS_SERVER_IP=10.0.0.10  # specify in startup parameter `--cluster-dns` for containerized kubelet
export KUBE_SERVER=172.28.0.10 # your master server ip, you may change it

sed -e "s/{{ pillar\['dns_replicas'\] }}/${DNS_REPLICAS}/g;s/{{ pillar\['dns_domain'\] }}/${DNS_DOMAIN}/g;s/{kube_server_url}/${KUBE_SERVER}/g;" skydns-rc.yaml.in > ./skydns-rc.yaml

sed -e "s/{{ pillar\['dns_server'\] }}/${DNS_SERVER_IP}/g" skydns-svc.yaml.in > ./skydns-svc.yaml

echo " - Start SkyDNS -"

kubectl -s "$KUBE_SERVER:8080" --namespace=kube-system create -f ./skydns-rc.yaml
kubectl -s "$KUBE_SERVER:8080" --namespace=kube-system create -f ./skydns-svc.yaml --validate=false

echo "------------------"
